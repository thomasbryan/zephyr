from flask import Flask,make_response,request
import base64,html,json,os,requests,time
path = os.path.dirname(os.path.realpath(__file__))
cfg = path+'/config.json'
config = {}
if not os.path.isfile(cfg):
  with open(cfg, 'w') as f:
    json.dump(config, f)
else:
  with open(cfg) as f:
    j = f.read()
  config = json.loads(j)
static = path+'/static/'
if not os.path.isdir(static):
  os.mkdir(static)
public = static+'public/'
if not os.path.isdir(public):
  os.mkdir(public)
ssh = os.path.expanduser('~/.ssh/')
if not os.path.isdir(ssh):
  os.mkdir(ssh)
  os.chmod(ssh,0o700)

app = Flask(__name__)

@app.route('/')
def index():
  fp = open(path+'/ui.html','r',encoding='utf-8')
  res = fp.read()
  fp.close()
  return res

@app.route('/upload/',defaults={'files':None},methods=['POST'])
@app.route('/upload/<files>',methods=['POST'])
def make_upload(files):
  res = '-'
  try:
    d = str(base64.urlsafe_b64decode(files),'utf-8')
  except:
    d = ''
  if os.path.isdir(public+d):
    f = request.files['file']
    f.save(os.path.join(public+d+f.filename))
    res = '+'
  return res

@app.route('/files/',defaults={'files':None},methods=['POST'])
@app.route('/files/<files>',methods=['POST'])
def make_files(files):
  res = {}
  p = public
  try:
    d = str(base64.urlsafe_b64decode(files),'utf-8')
  except:
    d = ''
  if os.path.isdir(p+d):
    if 'name' in request.form.keys():
      t = None
      name = request.form.get('name')
      base = str(base64.urlsafe_b64encode((d+name).encode('utf-8','surrogatepass')),'utf-8')
      if name[-1] == '/':
        if not os.path.isdir(p+d+name):
          os.mkdir(p+d+name)
          t = 'd'
      else:
        if not os.path.isfile(p+d+name):
          fp = open(p+d+name,'w')
          fp.write('')
          fp.close()
        t = 'f'
      if t:
        res['name'] = base
        res['type'] = t
      #else: error
  return json.dumps(res)

@app.route('/files/',defaults={'files':None})
@app.route('/files/<files>')
def list_files(files):
  dp = public
  try:
    d = str(base64.urlsafe_b64decode(files),'utf-8')
  except:
    d = ''
  if not os.path.isdir(dp+d):
    d = ''
  l = 100
  if 'l' in request.args.keys():
    l = int(request.args.get('l'))
  p = 0
  if 'p' in request.args.keys():
    p = int(request.args.get('p'))
  i = sorted(os.listdir(dp+d),key=str.casefold)
  if 'e' in request.args.keys():
    e = []
    for a in i:
      if a.endswith(request.args.get('e')):
        e.append(a)
    i = e
  tt = len(i)
  r = []
  for j in i[(p*l):(p*l)+l]:
    t = 'f'
    u = ''
    if os.path.isdir(dp+d+j):
      j += '/'
      t = 'd'
      u = 's'
    b = str(base64.urlsafe_b64encode((d+j).encode('utf-8','surrogatepass')),'utf-8')
    r.append({'n':j,'t':t,'u':u,'b':b})
    #name,type,url file/files, base64
  c = len(r)
  return {'d':d,'l':l,'p':p,'r':r,'c':c,'t':tt}

@app.route('/file/',defaults={'f':''})
@app.route('/file/<f>')
def show_file(f):
  try:
    base = str(base64.urlsafe_b64decode(f),'utf-8')
  except:
    base = ''
  if os.path.isfile(public+base):
    #check extension
    fp = open(public+base,'r',encoding='utf-8')
    c = fp.read()
    fp.close()
    return {'base':base,'data':c}
  return ''

@app.route('/files/<f>',methods=['PATCH'])
def edit_files(f):
  res = '-'
  try:
    base = str(base64.urlsafe_b64decode(f),'utf-8')
  except:
    base = ''
  if os.path.isfile(public+base):
    if 'data' in request.form.keys():
      fp = open(public+base,'w',encoding='utf-8')
      fp.write(request.form.get('data'))
      fp.close()
      res = '+'
  return res

@app.route('/files/<f>',methods=['DELETE'])
def raze_files(f):
  res = '-'
  try:
    base = str(base64.urlsafe_b64decode(f),'utf-8')
  except:
    base = ''
  if os.path.isfile(public+base):
    os.remove(public+base)
    res = '+'
  return res

'''
get:::
check that config.json exists
if not then check that load is in .ssh/config
load  ~/.ssh/zephyr < check that application file is loaded in.
create ~/.ssh/zephyr and save empty object to config.json
post:::
use input to add to config.json
if file is imported private key then verify public key
or create key.
get public key::
ssh-keygen -y -f ~/.ssh/ profile 
create private key::
ssh-keygen -t rsa -C "" -N "" -f ~/.ssh/profile -q

loop through config.json and create ~/.ssh/zephyr
#build out config.json based on application input

host zephyr-host
hostname hostname
user git
identityfile ~/.ssh/zephyr-host
'''

@app.route('/conf')
def show_conf():
  return config

@app.route('/conf',methods=['POST'])
def make_conf():
  res = '-'
  #set config variables
  return res

@app.route('/repos')
def show_repos():
  res = ''
  #get list of git repos in public
  return res

#git clone
#git branch
#git status
#git log
#git commit
#git push
#git pull

def icon(icon):
  if not os.path.exists(static+icon+'.png'):
    i = requests.get('http://openweathermap.org/img/wn/'+icon+'@2x.png')
    if i.status_code == 200:
      with open(static+icon+'.png','wb') as f:
        f.write(i.content)
  return icon+'.png'

@app.route('/weather')
def show_weather():
  res = {}
  if "weather" in config:
    if "api" in config["weather"]:
      api = config["weather"]["api"]
      exc = 'minutely,hourly'
      if "exc" in config["weather"]:
        exc = config["weather"]["exc"]
      uni = 'imperial'
      if "uni" in config["weather"]:
        uni = config["weather"]["uni"]
      if "places" in config["weather"]:
        cf = path+'/weather.json'
        ts = 0
        if os.path.isfile(cf):
          ts = os.path.getmtime(cf)
        if(int(time.time())-int(ts))>3600:
          if ts > 0:
            #TODO write to file.
            os.remove(cf)
        else:
          f = True
        try:
          with open(cf) as f:
            j = f.read()
        except IOError:

          #TODO loop through the places
          #places = config["weather"]["places"]
          lat = '33.077774'
          lon = '-96.652232'

          url = 'https://api.openweathermap.org/data/2.5/onecall?lat='+lat+'&lon='+lon+'&units='+uni+'&exclude='+exc+'&appid='+api
          h = requests.get(url)

          c = open(cf,'w')
          c.write(h.text)
          c.close()
          j = h.text
        try:
          resp = json.loads(j)
        except IOError:
          err = 'not json?'
        if resp:
          d = time.strftime("%A %B %d", time.localtime(resp["current"]["dt"]))
          t = str(int(resp["current"]["temp"]))
          weather = icon(str(resp["current"]["weather"][0]["icon"]))
          forecast = []
          for v in resp["daily"][0:5]:
            day = {
              "day": time.strftime("%a %d", time.localtime(v["dt"])).upper(),
              "high": str(int(v["temp"]["max"])),
              "low": str(int(v["temp"]["min"])),
              "weather": icon(str(v["weather"][0]["icon"])),
              "rise": time.strftime("%H:%M %p", time.localtime(v["sunrise"])),
              "set": time.strftime("%I:%M %p", time.localtime(v["sunset"]))
            }
            forecast.append(day)
        now = time.localtime()
        fresh = str(((59-now[4])*60)+(60-now[5]))
        return {'refresh':fresh,'date':d,'temp':t,'weather':weather,'forecast':forecast}
      else:
        res['error'] = 'no weather places'
    else:
      res['error'] = 'no weather api'
  return res

@app.route('/favicon.ico')
def fav():
  return ''

@app.route('/cmd')
def show_cmd():
  req = ''
  res = ''
  if 'cmd' in request.args.keys():
    req = request.args.get('cmd')
    res = os.popen(req,'r',1).read()
  return {'req':req,'res':res}
